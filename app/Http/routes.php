<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/personal-info', ['as' => 'personal-form', 'use' => 'PersonalInfoFormController@index']);
Route::post('/personal-info/{$id}', ['as' => 'personal-form-save', 'use' => 'PersonalInfoFormController@saveStep']);
Route::post('/personal-info', ['as' => 'personal-form-save', 'use' => 'PersonalInfoFormController@saveForm']);
