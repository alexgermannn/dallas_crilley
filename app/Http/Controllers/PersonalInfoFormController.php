<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class PersonalInfoFormController extends Controller
{
    public function index()
    {
		return view('personal-info-form');
    }

	public function saveStep(Request $request, $id)
	{
		$inputData = $request->all();
		$request->session()->put("form_step_{$id}", $inputData);
		$response = ['status' => 'success', 'msg' => 'Step saved'];

		return response()->json($response);
	}

	public function saveForm()
	{
		// TODO
	}
}
