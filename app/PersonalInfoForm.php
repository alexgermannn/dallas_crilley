<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PersonalInfoForm extends Model
{
	protected $table = 'personal_info_form';
	protected $guarded = ['id'];
}
