<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonalInfoFormTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personal_info_form', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->text('street_address');
            $table->string('apt_number');
            $table->string('city');
            $table->string('state');
            $table->string('zip');
            $table->string('date_of_birth');
            $table->integer('ssn_id');
            $table->string('email');
            $table->boolean('allow_info_use')->default(true);
            $table->string('phone');
            $table->integer('urban_code');
            $table->boolean('is_rural_address')->default(false);
            $table->boolean('add_shipping_address')->default(false);
            $table->boolean('is_shipping_temporary')->default(false);
            $table->text('shipping_street_address');
            $table->string('shipping_apt_number');
            $table->string('shipping_city');
            $table->string('shipping_state');
            $table->string('shipping_zip');
            $table->integer('shipping_urban_code');
            $table->string('promotional_code');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pesonal_info_form');
    }
}
